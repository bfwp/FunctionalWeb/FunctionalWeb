\chapter{Symbology\label{cha:symbology}}

\section{Symbols vs. Words}

So a lot of what's coming in the tools we'll be learning focus more on
symbols and composing them into expressions than on words and word
fragments. Now, in a lot of ways, this may seem off-putting. But
consider a few things before running away in shock and horror.

There's a word named \verb|map|. What's the first thing you think of
when you see that word? What are your guesses as to what that word
does in Haskell. Write down five things you think of in the margins.

Now, let's look at an example of a function using \verb|map|:

\lstinputlisting[linerange=21-22,caption=Simple ``\texttt{map}'' as a word example. application.]{FunctionalWebExamples/Source/Symbology/Map.hs}

What does this function do? We can run it and find out:

\begin{lstlisting}[caption=Run \texttt{mapSomething}]
> import Symbology.Map
> mapSomething dog ["Hello"]
["Hellodog"]
> mapSomething dog ["Hello", "Apple"]
["Hellodog","Appledog"]
\end{lstlisting}

What is this? We send in a list of strings, and get back a list of
strings with something added to each one? How is this like going from
Los Angeles to Gallup? Isn't that what you'd use a map for?

Well, the short version is yes, of course. If you have two places, and
want to go between them, you'd use a map. So if you have a list of
values, and want to change them in some way...

You're right. That makes no sense in any context outside of one where
``map'' doesn't mean what we've grown up believing it means.

So let's take a different look at the idea of ``mapping'' in
programming. And let's not call it ``mapping'' yet. When we're more
comfortable with the programming concept, it'll be easier not to be
put off by the differences. But that's okay. For now, we don't need to
use the word. Let's use a brand new symbol we're going to make
up. Let's call it
\verb|<$>|. I mean really, no one would use that in a
conversation. It's perfect.

So now, let's take the same function, but let's forget "map"
entirely. Let's just call the function $f$. I mean why not, right?
It's another letter that's just out there, with a lot less baggage
than "map".

\lstinputlisting[linerange=24-25,caption=Replacing words with symbols.]{FunctionalWebExamples/Source/Symbology/Map.hs}

Now, if we run $f$ the exact same way, how is it different?

\begin{lstlisting}
> import Symbology.Map
> f ["Hello"]
["Hellodog"]
> f ["Hello", "Apple"]
["Hellodog","Appledog"]
\end{lstlisting}

It isn't! But it's also a lot less surprising because there's no
inherent cultural information in \verb|<$>|, whereas in \verb|map|
literally everyone has a slightly different (or dramatically
different) idea of what maps are for.

So what does this symbol represent?

Let's look at another example. This time, we'll use something other
than a list. We'll use \verb|Maybe|. Now, yes, \verb|Maybe| is a word,
and it has a lot of loaded baggage too. But in this case, the baggage
is much, much closer to what the word actually means in this
context. We'll talk about that in a bit. But first:

\begin{lstlisting}
> import Symbology.Map
> f <$> Just "Hello"
Just "Hellodog"
> f <$> Just "Apple"
Just "Appledog"
\end{lstlisting}

Now, clearly, there's something about this symbol, a certain pattern,
a certain commonality. But what is it? Try writing a few notes in the
margins about it. And then continue reading.

\newpage

Before we figure that out though, let's try something else. Let's use
\verb|map| with \verb|Maybe|.

\begin{lstlisting}
> import Symbology.Map
> f <$> Just "Hello"
 <interactive>:15:16: error:
     • Couldn't match expected type ‘[String]’
                   with actual type ‘Maybe [Char]’
      • In the second argument of ‘($)’, namely ‘Just "Apple"’
       In the expression: mapSomething $ Just "Apple"
       In an equation for ‘it’: it = mapSomething $ Just "Apple"
\end{lstlisting}

What? Why? Aren't \verb|map| and
\verb|<$>| the same idea? What's going on here?

So as it turns out, the \verb|map| function not only uses a word with
a lot of baggage, it also uses it in a way that even makes
mathematical purists cringe. It does perform a similar operation as
\verb|<$>|, but it's much more specific. It's also older. And,
incidentally, is in the \verb|Prelude|. Which is another thing we'll
get to.

Let's find out about what the types of these functions are. I know we
haven't talked about types yet. So here's a brief introduction, with
some examples we've already seen.

%%% Local Variables:
%%% mode: latex
%%% TeX-master: "../FunctionalWeb"
%%% End:
