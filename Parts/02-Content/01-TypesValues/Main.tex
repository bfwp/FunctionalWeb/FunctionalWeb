\chapter{Types and Values\label{cha:types}}

\section{What is a Value?\label{sec:fandv}}

In Haskell, every value has a type. Functions even, which are also
just values, have types. There are also kinds of types, which means we
can express a lot of what we want a program to do as types, and then
by the time the program is run, most of the work is done, and
validated, and will be mostly what we'd hoped.

Another thing about types is that when writing programs, functions and
values have to use types in specific ways, so that they fit
together. It's like joining pipes, or putting together the pieces of a
puzzle in many ways. If each part doesn't match where it's connecting
to another, you need to get an adapter. We'll build just such an
adapter in this chapter, and show how the standard adapter itself must
be universal, and also how to use specialized adapters with a
universal wrapper.

And of course, we'll show practically just how powerful this kind of
generality can be, especially when the language itself has enough
information to tell you if your structure is going to hold up, or if
it's going to come crumbling down at the first sign of trouble.

\input{Parts/02-Content/01-TypesValues/01-ScalarTypes.tex}

\section{Nullary Functions\label{sec:types:nullary}}

Now, we've seen a few examples of \Types, which are really
well-defined sets, and \textit{values}, which are the individual
members of these sets.

Another way to think about \textit{values} is to think of them as
\glspl{nullary}. They're not just \textit{values}, they're also
\textit{functions} that don't have any parameters (nullary), and when
called always return the same \textit{value}. \footnote{In fact, this
  is one of the ways to define positive numbers starting from $0$,
  called
  \href{http://beastie.cs.ua.edu/proglan/readings/church.pdf}{Church
    Numerals.}} We can actually use this as a starting point of
composing larger and more detailed programs from smaller ones.

Now, we have two different kinds of ``spaces'' we can use when talking
about how a program should work. We have the space of \Types,
which establishes just what the combinations of sets of
\textit{values} a program accepts and produces, and we also have the
spaces these \Types\ define, each which contains its own space of
\textit{values}. Being able to talk about these separately means that
we can talk about how a program will behave using how the different
\Gls{type} spaces are connected, and then know that when it runs,
because we know the properties of the \textit{values} and how they're
related, it'll have well-understood behavior.

\section{Type Information\label{sec:typeinfo}}

So, let's fire up \gls{ghci} and take a look at these spaces with the
same \textit{values} we used in \hyperref[lst:scalars:values]{our
  earlier session}. This time, though, we'll give them
names\footnote{``Names'' are also called ``points'', and to a large
  extent, the more comfortable you get with \Types, the less use
  you'll make of ``points''. The language can tell what the parameters
  would be, and if all your function does is pass them on to another
  function, the compiler can automatically do this. This is called
  \gls{currying}, and Haskell handles this automatically.}to make it
easier to work with them:

\begin{listing}
  \begin{haskellcode}
  let i = 1182 :: Integer
  let c = 'a'
  let b = True
\end{haskellcode}
\end{listing}

What we've done here is define three \glspl{nullary}, $\{i, c, b\}$,
each returning a \textit{value} of its respective type,
$\{{Integer}, {Char}, {Bool}\}$. This is what the
\haskellinline|let| keyword does in \gls{ghci}.

Then, we used the special \gls{ghci} command \verb|:t| on each of
these functions. The \verb|:t| command asks \gls{ghci} to show us the
\Gls{type} information of a named value. It displays this using the
same format we'll use later when we define functions and rely more on
their \Types\ to understand the behavior of our
programs.\footnote{This is sometimes called ``Type-Driven
  Development'', where the \Types\ of a function are specified
  first, and then an implementation that satisfies them is
  written. While understanding how \Types\ help define the
  behavior of any given program, much of programming is exploratory,
  and just as often, we won't know the \Types\ of a function
  \textit{we've just written}. Having \gls{ghci} nearby to ask what it
  is we just did is then invaluable, because we can see the
  \Types\ inferred by the compiler, and understand whether what
  we wrote is what we intended.} In fact, with many of the more useful
extensions of modern Haskell, there are cases where the compiler can't
determine the \Types\ of a function without help from us, and
other cases where we aren't sure what the \Types\ should be, and
being able to take a function in isolation and interact with
\gls{ghci} to refine the \Types\ and implementation is really
handy.

%%% Local Variables:
%%% mode: latex
%%% TeX-master: "../../../FunctionalWeb.tex"
%%% End:
