\chapter*{Installing Software}

\section*{Required Software}

For people comfortable finding and installing software, here's a list
of what's required:

\begin{itemize}
\item Haskell, of course. We make use of many of the more advanced
  features of \gls{ghc8}, so you'll need that plus at least
  \gls{cabal124}. There are
  \href{https://www.haskell.org/ghc/download_ghc_8_0_2.html}{downloaders
    available for most platforms} from the GHC team.
\item The \gls{icu57} or newer, although we haven't tested with \gls{icu59} yet.
\item \gls{gmake} which we use to configure the exercises, and which
  several other packages will use for building themselves.
\item \gls{pkg-config}, which maintains a repository of settings that
  other libraries can use to find where their dependencies are and
  pass instructions along to the compiler.
\end{itemize}

\section*{Platform Instructions}

\subsection*{Ubuntu Linux}

There's a \gls{ppa-hvr} which has the latest \gls{ghc} and \gls{cabal}
versions. In installs them under \verb|/opt/ghc| and integrates them
into the standard \gls{alternatives} system. To install this PPA, and
the other required packages, run these commands:

\begin{listing}
  \begin{bashcode}
    sudo add-apt-repository ppa:hvr/ghc
    sudo apt-get update
    sudo apt-get install build-essential libicu-dev pkg-config
    sudo apt-get install ghc-8.0.2 cabal-1.24
    sudo update-alternatives --set opt-ghc /opt/ghc/bin/ghc-8.0.2
    sudo update-alternatives --set opt-cabal /opt/cabal/bin/cabal-1.24
  \end{bashcode}
  \caption{Installing \gls{ppa-hvr} on Ubuntu.\label{lst:start:ubu:hvr}}
\end{listing}

This will add the repository and it's package key, fetch the new list
of packages, install \gls{ghc8}, \gls{cabal124}, \gls{gmake},
\gls{git}, \gls{pkg-config}, and several other key tools.

\subsection*{Mac OS}

\subsubsection*{Xcode Command Line Tools}

First, you'll need to install
\href{https://developer.apple.com/xcode/}{Xcode}, which will also
enable the
\href{https://developer.apple.com/library/content/technotes/tn2339/_index.html#//apple_ref/doc/uid/DTS40014588-CH1-WHAT_IS_THE_COMMAND_LINE_TOOLS_PACKAGE_}{Command
  Line Tools}. These are used internally by \gls{ghc}, and also when
installing the libraries many of the other packages Haskell uses.

\subsubsection*{MacPorts or Homebrew for ICU}

You'll also need to install \gls{icu}. There are two commonly-used
package managers that make this easier, and either will work:

\begin{enumerate}
\item \gls{ports}: \href{https://www.macports.org/}{This is the
    recommended solution}. It doesn't override system files, does
  extensive testing, uses released and vetted versions, and even has a
  binary packaging system so you're not downloading and building the
  same sources as everyone else.
\item \gls{brew}: \href{https://brew.sh/}{This is the popular
    solution}. It takes over a common system directory, doesn't
  integrate well with other system packages, and makes actually using
  the development portions of packages much more difficult than
  necessary. But being popular, if things do go wrong, and you catch
  the community on the right day, they might just answer any questions
  you have.
\end{enumerate}

\subsubsection*{MacOS with MacPorts}

Now, with \gls{ports}, to install the tools, first update the ports to
make sure you're running the most recent version. These don't have to
be run again until you're feeling more comfortable with third-party
libraries, and aside from \gls{icu}, we don't really depend on any
others:

\begin{listing}
  \begin{bashcode}
    sudo port selfupdate
    sudo port upgrade outdated
  \end{bashcode}
  \caption{Updating \gls{ports} to the latest version.}
  \label{lst:macos:ports:update}
\end{listing}

And then you can install \gls{icu}, and \gls{gmake}:

\begin{listing}
  \begin{bashcode}
    sudo port install icu gmake pkg-config
  \end{bashcode}
  \caption{Installing \gls{icu} and \gls{gmake} using \gls{ports}.}
  \label{lst:macos:ports:packages}
\end{listing}

And that's it. \gls{ports} uses it's own location on your system to
store packages, and \gls{pkg-config} to let other software find it, so
there's no extra configuration to manage.

\subsubsection*{MacOS with Homebrew}

So \gls{brew} takes over what's normally considered a place for any
system local packages, \verb|/usr/local|. It also is very particular
that you haven't insalled anything else through another installer, and
will announce ominous warnings if you do. It also hides key
configuration from \gls{pkg-config}, so we do some extra checks in our
build tools to make sure that we can find and build the libraries. For
now it's not a real concern, but as you get more skillful at
development, switching to \gls{ports} will definitely make many things
easier.

Now, to install what we need:

\begin{listing}
  \begin{bashcode}
    brew install pkg-config
    brew install make
    brew install icu4c
  \end{bashcode}
  \caption{Installing \gls{gmake}, \gls{icu}, and \gls{pkg-config} usinng \gls{brew}.}
  \label{lst:macos:brew}
\end{listing}

And for now, that's basically it. Our package tools should find
everything at this point.

%%% Local Variables:
%%% mode: latex
%%% TeX-master: "../../FunctionalWeb"
%%% End:
