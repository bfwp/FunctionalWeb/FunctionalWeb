\newglossaryentry{hask}{%
  name={Haskell},
  description={The \href{https://www.haskell.org/}{Haskell}
    programming language is a complied, strongly-typed, lazily-evaluated
    language that has a broad set of libraries and, with many of the
    recent developments, is also a powerful tool for people of all
    backgrounds to learn about how computers work their magic, and how to
    make them do things you'd like.
  }
}

\newglossaryentry{ghci}{
  name={ghci},
  text={\texttt{ghci}},
  parent={hask},
  description={\href{https://wiki.haskell.org/GHC/GHCi}{The Haskell
      Shell}, also known as \texttt{ghci}, is an interactive system for
    running many different kinds of Haskell code. All of our examples are
    written so that they can be run this way, and you can interact and
    change things with them live and see what the effects are.
  }
}

\newglossaryentry{systools}{
  name={System Tools},
  description={These are various common platform tools that are used
    by nearly everything running on a system.
  }
}

\newglossaryentry{shell}{
  parent={systools},
  name={shell},
  description={The \texttt{shell} is any of the various interpreter
    programs that your system starts when running a console or terminal
    application. Most systems run a shell called
    \href{https://www.gnu.org/software/bash/}{\texttt{bash}}, which is a
    rewrite of the original interpreter, \texttt{sh}. Many people change
    this to use another shell, usually one of
    \href{http://www.zsh.org/}{\texttt{zsh}},
    \href{http://fishshell.com/}{\texttt{fish}}, or
    \href{https://en.wikipedia.org/wiki/Tcsh}{\texttt{tcsh}}. Whichever
    shell you use, most of what will happen will be in \gls{ghci} or in
    compiled Haskell code, and the differences won't matter much.}
}

\newglossaryentry{type}{
  parent={hask},
  name={type},
  description={A \textit{Type} is a set or combination of sets that
    sit ``atop'' the actual values a program uses. The compiler uses these
    sets to ensure that when values are used by the program, they're
    compatible. The \textit{Type System} will follow the extent of the
    program to check that all uses are valid, or if there are invalid
    uses, or uses the compiler can't determine, it will produce an error
    explaining what it found, and quite often, how to fix it.}
}

\newglossaryentry{scalar}{
  parent={type},
  name={scalar},
  description={A \textit{Scalar Type} is a Type that has only a single
    set of values that it can have. Examples include \textit{Integer},
    \textit{Char}, \textit{Bool}, and any other simple type which can't
    have another value ``extracted'' from it or transformed inside it.}
}

\newglossaryentry{unicode}{
  name={Unicode},
  text={\textit{Unicode}},
  description={\textit{Unicode} is an international standard that
    defines \glspl{codepoint}. It's overseen by the
    \href{http://www.unicode.org/}{\textit{Unicode Consortium}}. The
    current version is
    \href{http://www.unicode.org/versions/Unicode10.0.0/}{Version 10},
    which includes new writing scripts, emojis, currency markers, and
    updates to sorting rules.
  }
}

\newglossaryentry{codepoint}{
  parent={unicode},
  name={Code Point},
  description={A \textit{Code Point} is a number associated with a
    text element in \gls{unicode}. It represents a single letter, number,
    symbol, character, or a control code for combining other \textit{Code
      Points} into combination characters. The Haskell \texttt{Char} type
    represents individual \textit{Code Points}%
  }
}

\newglossaryentry{nullary}{
  parent={type},
  name={Nullary Function},
  description={A \textit{Nullary Function} is a function that doesn't
    take any parameters, and usually (but not necessarily) returns the
    same value each time it's called. These are how Haskell represents
    values of \glspl{scalar}.
  }
}

\newglossaryentry{currying}{
  parent={type},
  name={Currying},
  description={\textit{Currying} a function is taking a function that
    expects more than one argument, providing values for the first
    arguments, but leaving one or more of the last unspecified, and
    creating a new function that only expects the remaining
    arguments. Some languages, like Haskell, do this automatically, while
    others need explicit wrapping functions. Either way, the process isn't
    innate to any particular language or paradigm, although having
    automatic support for it in the language does make it much more
    convenient.}
}

\newglossaryentry{funcref}{
  parent={type},
  name={Functional Reference},
  text={\textit{Functional Reference}},
  description={A
\href{https://twanvl.nl/blog/haskell/overloading-functional-references}{\textit{Functional
Reference}} is a special kind of value that when combined with a
generic “forwards” or “backwards” function and a corresponding value
will return a part of the value, or return a copy of the value with a
part changed. Since they encode type information, even with the
generic operator functions they can be composed in arbitrarily complex
ways while still being completely safe at compile-time.
  }
}

\newglossaryentry{partialfunc}{
  parent={type},
  name={Partial Function},
  text={\textit{Partial Function}},
  description={A \textit{Partial Function} is a function that isn’t
    defined for the entire set of inputs it’s types accept, and so while
    it might compile with no warnings or errors, the function can still
    cause errors at runtime. Generally, avoiding them is the right thing
    to do, and for the most part, this book and exercises don’t use them
    except when making a specific point.
  }
}

\newglossaryentry{iana}{
  name={IANA},
  first={Internet Assigned Numbers Authority (IANA)},
  description={The \href{https://www.iana.org/}{Internet Assigned
      Numbers Authority} coordinates the assignment of global internet
    resources, such as IP addresses, protocol and port numbers, top-level
    domain names, and other unique identifiers.}
}

\newglossaryentry{ghc}{
  parent={hask},
  name={GHC},
  first={Glasgow Haskell Compiler},
  description={\href={https://www.haskell.org/ghc/}{The Glasgow
      Haskell Compiler} is one of several compilers for the \gls{hask}
    language. It's the most active one currently, and is the one we're
    targeting for this book.}
}
\newglossaryentry{ghc8}{
  parent={hask},
  name={GHC 8},
  first={Glasgow Haskell Compiler Version 8 (GHC)},
  description={%
    \href{https://downloads.haskell.org/~ghc/8.0.2/docs/html/users_guide/index.html}{GHC
      8} is a fairly extensive reworking of GHC, and includes many
    improvements in the type system. Our core library makes heavy use of
    other packages which depend on these improvements to simplify writing
    code that's more thoroughly checked at compile-time.
  },
  see={ghc}
}

\newglossaryentry{cabal}{
  parent={hask},
  name={Cabal Install},
  description={Cabal Install is the command used to manage Haskell
    packages. Even our example code is distributed as a local Cabal
    package, and so it's possible to use Cabal to build and run these much
    easier than trying everything manually. No one has done things
    manually like that for quite a long time, and we're not about to ask
    you to, either.}
}

\newglossaryentry{cabal124}{
  parent={hask},
  name={Cabal 1.24},
  first={Cabal Install Version 1.24},
  description={\href{http://hackage.haskell.org/package/cabal-install-1.24.0.2}{Cabal
      Install 1.24} is another extensive reworking of a key component of
    Haskell, and supports the enhanced package management of
    \gls{ghc8}. Both of these are required together, but Cabal Install
    1.24 can run with earlier versions of GHC, so it's possible to get
    both runninng without being stuck in a circle.},
  see={cabal}
}

\newglossaryentry{ppa-hvr}{
  parent={systools},
  name={HVR's PPA},
  first={Herbert V. Riedel's Haskell Package Archive},
  description={Herbert V. Riedel
    \href{https://launchpad.net/~hvr/+archive/ubuntu/ghc}{maintains an
      Ubuntu PPA} containing the latest builds of \gls{ghc8} and
    \gls{cabal124}. This makes installation on Ubuntu much easier.
  },
  see={alternatives}
}

\newglossaryentry{git}{%
  parent={systools},
  name={git},
  text={\texttt{git}},
  description={\href{https://git-scm.com/}{\texttt{git}} is a tool for
    managing changes in projects. It has excellent support for
    collaborative use, even when people have made changes to the same
    files. There are also many third-party resources for helping mange
    \texttt{git} repositories.}
}

\newglossaryentry{icu}{%
  parent={unicode},
  name={icu},
  first={International Components for Unicode}
  text={\textit{ICU}},
  description={The
    \href{http://site.icu-project.org/home}{\textit{International
        Components for Unicode}} is a common library published by the
    \gls{unicode} Consortium for third parties to use for interpreting the
    various forms of \gls{unicode} text.
  }
}

\newglossaryentry{gmake}{%
  parent={systools},
  text={\texttt{gmake}},
  name={gmake},
  first={GNU Make (\texttt{gmake})},
  description={\href{https://www.gnu.org/software/make/}{GNU Make} is
    a program that uses a series of descriptive instructions and
    interdependencies between them to figure out what's done, what isn't
    and then run whatever's left to build software.
  }
}

\newglossaryentry{ports}{%
  parent={systools},
  name={MacPorts},
  description={\href{https://www.macports.org/}{MacPorts} is a
    collection of software packages and management tools for MacOS. It
    manages installing requested packages and others they depend on,
    upgrading them cleanly, and cleanly removing them. It's also
    well-integrated with MacOS, uses the system frameworks where
    available, and supports several global and local configuration
    options.%
  }
}

\newglossaryentry{brew}{
  parent={systools},
  name={Homebrew},
  description={\href{https://brew.sh/}{Homebrew} is another package
    manager for MacOS. It's slightly less well-integrated, not very
    careful about how it managed dependencies, and has managed to acquire
    a huge following despite all this. Usually if you've gone through a
    code camp or have a bit of experience with other programmers, you'll
    have already installed this, but if you haven't yet, installing
    \gls{ports} is going to be much easier to manage.
  }
}

\newglossaryentry{icu57}{%
  parent={unicode},
  name={icu57},
  first={International Components for Unicode, Version 5.7},
  text={\textit{ICU 5.7}},
  description={Version 5.7 of \gls{icu}.},
  see={icu}
}

\newglossaryentry{icu59}{%
  parent={unicode},
  name={icu59},
  first={International Components for Unicode, Version 5.9}
  text={\textit{ICU 5.9}},
  description={Version 5.9 of \gls{icu}.}
  see={icu}
}

\newglossaryentry{pkg-config}{%
  parent={systools},
  name={\texttt{pkg-config}},
  description={\href{https://www.freedesktop.org/wiki/Software/pkg-config/}{\texttt{pkg-config}}
    is a configuration repository that packages can use to register
    themselves so that other build tools can find their information. It's
    especially used when compiling software that requires other libraries
    to find out the flags the compiler needs to find interfaces and
    compiled modules to assemble into a finished program.
  }
}

\newglossaryentry{alternatives}{
  parent={systools},
  name={\texttt{alternatives}},
  description={The
    \href{https://debian-administration.org/article/91/Using_the_Debian_alternatives_system}{Debian
      Alternatives System} is a core component of several distributions. It
    allows packages to register as providers for common services, register
    their own services, and select between versions. It's quite flexible
    in what it can configure, from generic service names to highly
    specific separate package versions that can coexist on a system.
  }
}

\newglossaryentry{module}{
  name={Haskell Module},
  description={Haskell separates code into modules. Modules are
    individual files, and their path relative to the project root should
    be consistent with the \gls{cabal} configuration for the
    project. Modules define their own lists of exported values,
    documentation, and reexported values. By limiting exports, it's
    possible to define interfaces very well for other modules, while still
    having potentially unsafe code internally.
  }
}

\newglossaryentry{modhead}{
  parent={module},
  name={Module Header},
  description={Each module should have a
    \href{https://www.haskell.org/haddock/doc/html/ch03s03.html}{Module
      Header} (or \textit{Module Description}). This lists essential
    information, such as the name of the module, what it does, copyright
    and licensing information, how to contact the maintainer, and other
    useful information.
  }
}

\newglossaryentry{haddock}{
  parent={module},
  name={haddock},
  text={\textit{Haddock}},
  description={Haskell has a special comment mark-up format,
    \href{https://www.haskell.org/haddock/doc/html/index.html}{\textit{Haddock}}. This
    can be processed by \gls{cabal} on installation and included in the
    local module documentation repository, and browsed, along with
    crossreferences to the source, using a web browser.}
}

\newglossaryentry{explist}{
  parent={module},
  name={Export List},
  text={\textit{Export List}},
  description={The \textit{Export List} of a \gls{module} lists the
    types and values that the module exports. Managing the \textit{Export
      List} to hide internal functions and type constructors which exposing
    only smart helper functions and other functional references is a good
    practice for defining stable interfaces for other modules to use.
  }
}

\newglossaryentry{package}{
  name={Haskell Package},
  description={A Haskell Package is a collection of modules and a
    configuration file describing to \gls{cabal} how to build and install
    it. It includes programs, libraries, and other non-Haskell resource
    files.
  }
}

\newglossaryentry{liblawless}{
  parent={package},
  name={liblawless},
  text={\texttt{liblawless}},
  description={\href{http://hackage.haskell.org/package/liblawless}{\texttt{liblawless}}
    is the Haskell \gls{package} we wrote to make using modern Haskell
    much easier. It includes a set of default modules, a much improved IO
    model, and support for bidirectional human-readable serialization. All
    standard functions are safe at both compile and runtime, unlike the
    default \gls{prelude}.
  }
}

\newglossaryentry{prelude}{
  parent={package},
  name={prelude},
  text={\texttt{Prelude}},
  description={The
    \href{http://hackage.haskell.org/package/base-4.9.1.0/docs/Prelude.html}{Standard
      \texttt{Prelude}} is a collection of libraries that over the years has
    grow, shrunk, and essentially retained as much compatibility with the
    \gls{hask98} standard as possible. It’s riddled with functions that
    will gladly compile, but then throw exceptions at runtime under common
    inputs. Most projects replace it with anything else after a short
    period. Ours is \gls{liblawless}, and is deliberately composed of a
    much different set of \glspl{package}.
  }
}

\newglossaryentry{hask98}{
  parent={hask},
  name={Haskell ‘98},
  description={\href{https://www.haskell.org/onlinereport/}{Haskell
      1998} is the version of the \gls{hask} language established to
    \href{https://www.haskell.org/onlinereport/preface-jfp.html}{consolidate
      several different versions} of the original language. It was
    published in 1998, with work on library standardization published
    in 1999. However, in the interest of simplifying the standard
    library, there are numerous regressions from earlier versions,
    including a large number of
    \href{https://wiki.haskell.org/List_of_partial_functions}{commonly
      used but runtime-error-inducing functions}.
  }
}

\newglossaryentry{lens}{
  parent={package},
  name={lens},
  text={\texttt{lens}},
  description={The
    \href{http://hackage.haskell.org/package/lens}{\texttt{lens}}
    \gls{package} is a full complement of various \gls{funcref}
    combinators, operators, and functions to make composing complex data
    structures much simpler and less error-prone than explicit pattern
    matching. They also provide a way to use the \gls{explist} to provide
    a fixed and safe interface to a complex library that may need to do
    many unsafe operations internally, but is fully safe with the right
    interface.
  }
}


%%% Local Variables:
%%% mode: latex
%%% TeX-master: "../FunctionalWeb.tex"
%%% End:
