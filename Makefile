# Just use one shell for each action group, and make sure it's bash,
# and that it stops on error.
.ONESHELL:
SHELL := bash

# Fail if any command has an error
.SHELLFLAGS := -o pipefail -ec

PROJECT = FunctionalWeb
PROJECT_BOUND = $(PROJECT)Bound

MAINTEX = $(PROJECT).tex
BOUNDTEX = $(PROJECT_BOUND).tex

TARGET = $(PROJECT).pdf
TARGET_BOUND = $(PROJECT_BOUND).pdf

TEXFILES = $(wildcard *.tex)
GLOSSARIES = Glossaries
TERMS = Terms
ACRONYMS = Acronyms

GLOSSARIES_ALL = $(TERMS) $(ACRONYMS)

GLS_GLO = $(PROJECT).glo
GLS_GLS = $(PROJECT).gls
GLG_GLG = $(PROJECT).glg

ACR_ACN = $(PROJECT).acn
ACR_ACR = $(PROJECT).acr
ACR_ALG = $(PROJECT).alg

GLS_SRCS = $(GLOSSARIES_ALL:%=$(GLOSSARIES)/%.tex)

GLS_TARGETS = \
	$(GLS_GLO) $(GLS_GLS) $(GLS_GLG) \
	$(ACR_ACN) $(ACR_ACR) $(ACR_ALG)

BCF = $(PROJECT).bcf
BIBER = $(PROJECT).bbl $(PROJECT).bcb

LOE = $(PROJECT).loe

MINTED_CACHE = _minted-$(PROJECT)

# Run XeLaTeX with filters for errors, and then filtering the logs for
# signs we need to run it again, and if so, touch .runagain so it's
# newer that any output.
#
# First run xelatex. If it exits with an error, we grep for them and
# return an error.
#
# Next, grep the log to see if we need to run again, and if so, touch
# .runagain.
#
# Otherwise, return success because we're done.
LATEX = \
	echo Running xelatex; \
	if ! xelatex -shell-escape -interaction=nonstopmode -8bit \
		$(PROJECT).tex &> /dev/null; then \
		egrep ':[^:]*Warning:|^!' $(PROJECT).log; \
		false; \
	elif \
		egrep -q '(undefined references)|(Rerun to get cross)' \
			$(PROJECT).log; then \
		echo Will run again; \
		touch .runagain; \
	else \
		echo Complete; \
		true; \
	fi

LATEX_BOUND = \
	echo Running xelatex; \
	if ! xelatex -shell-escape -interaction=nonstopmode -8bit \
		$(PROJECT_BOUND).tex &> /dev/null; then \
		egrep ':[^:]*Warning:|^!' $(PROJECT_BOUND).log; \
		false; \
	elif \
		egrep -q '(undefined references)|(Rerun to get cross)' \
			$(PROJECT_BOUND).log; then \
		echo Will run again; \
		touch .runagain; \
	else \
		echo Complete; \
		true; \
	fi

# These don't output anything
.PHONY: clean all

# This is most definitely an intermediate file.
.INTERMEDIATE: .runagain

# Build the whole thing. A convenient default.
all: $(TARGET_BOUND)

$(TARGET_BOUND): $(TARGET)
	$(LATEX_BOUND)

# Keep going until we're done or dead.
$(TARGET): $(TEXFILES) $(GLOS) $(GLGS) $(GLSS) .runagain $(BIBER) $(LOE)
	$(LATEX)

# .runagain will be created here so we run $(TARGET).
.runagain $(GLS_TARGETS): $(GLS_SRCS)
	$(LATEX)
	@makeglossaries $(PROJECT)
	@touch .runagain

# The BibTeX control file gets created when we run LaTeX for the first
# time.
$(BCF): $(TEXFILES)
	$(LATEX)

$(BIBER): $(TEXFILES) $(BCF)
	@biber $(PROJECT)

clean:
	@rm -rf \
		$(TARGET) $(TARGET_BOUND) $(GLS_TARGETS) $(MINTED_CACHE) \
		*.log *.aux *.ist *.lot *.lol *.loe *.blg *.thm *.out *.toc *.xdy .runagain \
		*.bbl *.bcf *.run.xml \
